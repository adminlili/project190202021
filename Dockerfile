FROM node

RUN mkdir /skillbox

WORKDIR /skillbox

COPY . /skillbox

# RUN only when BUILD container
RUN yarn install 
RUN yarn test
RUN yarn build

# when container start!!! 
CMD yarn start
